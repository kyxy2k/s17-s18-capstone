package com.zuitt.discussion.Repositories;

import com.zuitt.discussion.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Object> {
    User findByUsername(String username);
}
