package com.zuitt.discussion.exceptions;

public class UserExceptions extends Throwable{
    public class UserException extends Exception{
        public UserException(String message){
            super(message);
        }
    }
}
